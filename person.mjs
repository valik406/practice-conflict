export default class Person {
    constructor(name) {
        this.name = name;
    }

    say(phrase) {
        return `${this.name} says ${phrase}`;
    }
        run(speed) {
        return `${this.name} runs ${speed}`;
    }
}

