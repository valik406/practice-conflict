import Person from "./person.mjs";

class Speaker extends Person {
    say(phrase) {
        console.log(`"${super.say(phrase)}" very confidently`)
    }
    run(speed) {
        console.log(`"${super.run(speed)}", but confidently`)
    }
}

let john = new Person('John');
console.log(john.say('Hello!'));
console.log(john.run('slowly'));

let bob = new Speaker('Bob');
console.log(bob.say('Hi!'));
console.log(bob.run('fast'));

